$(document).ready(function(){
    // VARIABLES Y OBJETOS
    const  MenuResponsive = {
        'active' : false,
        'margin' : 110,
        'menu' : $('.xnav-bar'),
        'menuLink' : $('.xnav-bar a'),
        'widthScreen' : 0,
        'setWidthScreen' : function(widthScreen){
            this.widthScreen = widthScreen;
        }
    };

    // FUNCIONES
    function innerWidthScreen(){
        MenuResponsive.setWidthScreen(window.innerWidth);
        if(MenuResponsive.widthScreen > 992){
            MenuResponsive.menu.css({
                'display' : 'grid',
                'margin-right' : 0
            });
            MenuResponsive.menuLink.css({ 'color' : '#fff' });
        }else{
            MenuResponsive.menu.removeAttr('style');
        }
        MenuResponsive.active = false;
    }

    // EVENTOS
    // mostrar y ocultar menu responsivo
    $('#xresponsive-menu').on('click', function(){
        if(!MenuResponsive.active){
            MenuResponsive.menu.show();
            MenuResponsive.menu.animate({ 'margin-right' : 0 });
            setTimeout(() => {
                MenuResponsive.menuLink.css({ 'color' : 'white' });
            }, 200);
            MenuResponsive.active = true;
        }else{
            MenuResponsive.menu.animate({ 'margin-right' :  110+'%' });
            setTimeout(() => {
                MenuResponsive.menuLink.css({ 'color' : 'transparent' });
            }, 150);
            MenuResponsive.active = false;
        }
    });

    window.addEventListener('resize', innerWidthScreen);

    innerWidthScreen();

});